
pub enum Class {
    Barbarian,
    Bard,
    Cleric,
    Druid,
    Fighter,
    Monk,
    Paladin,
    Ranger,
    Rogue,
    Sorcerer,
    Wizard,
}

pub fn get_bab(class: &Class, lvl: i32) -> i32 {
    // Base Attack Bonuses are either good (equal to class level),
    // average (3/4's of class level), or poor (half of class level)
    let average = |num: i32| { 3 * num / 4 };
    let poor    = |num: i32| { num / 2 };

    match class {
        &Class::Barbarian => lvl,
        &Class::Bard      => average(lvl),
        &Class::Cleric    => average(lvl),
        &Class::Druid     => average(lvl),
        &Class::Fighter   => lvl,
        &Class::Monk      => average(lvl),
        &Class::Paladin   => lvl,
        &Class::Ranger    => lvl,
        &Class::Rogue     => average(lvl),
        &Class::Sorcerer  => poor(lvl),
        &Class::Wizard    => poor(lvl),
    }
}

pub fn get_base_saves(class: &Class, lvl: i32) -> (i32, i32, i32) {
    // Base saves are either poor (One third the class' hitdice)
    // or good (half the class' hitdice plus two)
    let good = |num: i32| { num / 2 + 2 };
    let poor = |num: i32| { num / 3 };

    match class {
        &Class::Barbarian => ( good(lvl), poor(lvl), poor(lvl) ),
        &Class::Bard      => ( poor(lvl), good(lvl), good(lvl) ),
        &Class::Cleric    => ( good(lvl), poor(lvl), good(lvl) ),
        &Class::Druid     => ( good(lvl), poor(lvl), good(lvl) ),
        &Class::Fighter   => ( good(lvl), poor(lvl), poor(lvl) ),
        &Class::Monk      => ( good(lvl), good(lvl), good(lvl) ),
        &Class::Paladin   => ( good(lvl), poor(lvl), poor(lvl) ),
        &Class::Ranger    => ( good(lvl), good(lvl), poor(lvl) ),
        &Class::Rogue     => ( poor(lvl), good(lvl), poor(lvl) ),
        &Class::Sorcerer  => ( poor(lvl), poor(lvl), good(lvl) ),
        &Class::Wizard    => ( poor(lvl), poor(lvl), good(lvl) ),
    }
}

pub fn class_to_string(class: &Class, lvl: i32) -> String {
    match class {
        &Class::Barbarian => format!("{} {}", "Barbarian", lvl),
        &Class::Bard      => format!("{} {}", "Bard",      lvl),
        &Class::Cleric    => format!("{} {}", "Cleric",    lvl),
        &Class::Druid     => format!("{} {}", "Druid",     lvl),
        &Class::Fighter   => format!("{} {}", "Fighter",   lvl),
        &Class::Monk      => format!("{} {}", "Monk",      lvl),
        &Class::Paladin   => format!("{} {}", "Paladin",   lvl),
        &Class::Ranger    => format!("{} {}", "Ranger",    lvl),
        &Class::Rogue     => format!("{} {}", "Rogue",     lvl),
        &Class::Sorcerer  => format!("{} {}", "Sorcerer",  lvl),
        &Class::Wizard    => format!("{} {}", "Wizard",    lvl),
    }
}

pub fn from_string(class_str: &str, level: i32) -> Result<(Class), String> {
    match class_str {
        "barbarian" => Ok( Class::Barbarian ),
        "bard"      => Ok( Class::Bard ),
        "cleric"    => Ok( Class::Cleric ),
        "druid"     => Ok( Class::Druid ),
        "fighter"   => Ok( Class::Fighter ),
        "monk"      => Ok( Class::Monk ),
        "paladin"   => Ok( Class::Paladin ),
        "ranger"    => Ok( Class::Ranger ),
        "rogue"     => Ok( Class::Rogue ),
        "sorcerer"  => Ok( Class::Sorcerer ),
        "wizard"    => Ok( Class::Wizard ),
        _ => Err(class_str.to_string()),
    }
}

pub fn parse_class_args(class_vec: &mut Vec<(Class, i32)>,
                        err_vec:   &mut Vec<String>,
                        arg: &str) {
    let mut temp = false;
    let mut class: Class;
    let mut level: i32;

    for word in arg.split_whitespace() {
    }
}

// pub fn get_hit_dice(level: &Class) -> String {
