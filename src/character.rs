
use class::Class;
use class::class_to_string;
use class::get_bab;
use class::get_base_saves;

use std::mem;

pub enum SizeClass {
    Fine,
    Tiny,
    Small,
    Medium,
    Large,
    Huge,
    Gargantuan,
    Colossal,
}

pub fn size_to_string(size: &SizeClass) -> String {
    match size {
        &SizeClass::Fine       => "Fine".to_string(),
        &SizeClass::Tiny       => "Tiny".to_string(),
        &SizeClass::Small      => "Small".to_string(),
        &SizeClass::Medium     => "Medium".to_string(),
        &SizeClass::Large      => "Large".to_string(),
        &SizeClass::Huge       => "Huge".to_string(),
        &SizeClass::Gargantuan => "Gargantuan".to_string(),
        &SizeClass::Colossal   => "Colossal".to_string(),
    }
}


pub enum Race {
    Dwarf,
    Elf,
    Gnoll,
    Gnome,
    Goblin,
    HalfElf,
    Halfling,
    HalfOrc,
    Human,
    Kobold,
    LizardFolk,
    Minotaur,
    Ogre,
    OgreMage,
    Orc,
}

pub fn race_to_string(race: &Race) -> String {
    match race {
        &Race::Dwarf      => "Dwarf".to_string(),
        &Race::Elf        => "Elf".to_string(),
        &Race::Gnome      => "Gnome".to_string(),
        &Race::Goblin     => "Goblin".to_string(),
        &Race::Gnoll      =>  "Gnoll".to_string(),
        &Race::HalfElf    => "HalfElf".to_string(),
        &Race::Halfling   => "Halfling".to_string(),
        &Race::HalfOrc    => "HalfOrc".to_string(),
        &Race::Human      => "Human".to_string(),
        &Race::Kobold     => "Kobold".to_string(),
        &Race::LizardFolk => "LizardFolk".to_string(),
        &Race::Minotaur   => "Minotaur".to_string(),
        &Race::Ogre       => "Ogre".to_string(),
        &Race::OgreMage   => "OgreMage".to_string(),
        &Race::Orc        => "Orc".to_string(),

    }
}

pub fn race_from_string(race_str: &str) -> Result<Race, String> {
    match race_str {
        "dwarf"      => Ok( Race::Dwarf ),
        "elf"        => Ok( Race::Elf ),
        "gnome"      => Ok( Race::Gnome ),
        "goblin"     => Ok( Race::Goblin ),
        "gnoll"      => Ok( Race::Gnoll ),
        "halfelf"    => Ok( Race::HalfElf ),
        "halfling"   => Ok( Race::Halfling ),
        "halforc"    => Ok( Race::HalfOrc ),
        "human"      => Ok( Race::Human ),
        "kobold"     => Ok( Race::Kobold ),
        "lizardfolk" => Ok( Race::LizardFolk ),
        "minotaur"   => Ok( Race::Minotaur ),
        "ogre"       => Ok( Race::Ogre ),
        "ogremage"   => Ok( Race::OgreMage ),
        "orc"        => Ok( Race::Orc ),
        _ => Err( race_str.to_string() ),
    }
}

/*
pub enum Alignment {
    Unaligned,
    LawfulGood,
    NeutralGood,
    ChaoticGood,
    LawfulNeutral,
    TrueNeutral,
    ChaoticNeutral,
    LawfulEvil,
    NeutralEvil,
    ChaoticEvil,
}
*/

pub struct Character {
    name:   String,
    level:  Vec<(Class, i32)>,
    size:   SizeClass,
    race:   Race,
    // align:  Alignment,
    abilities:  [i32; 6],   // [Str, Dex, Con, Int, Wis, Cha]
    saves:      [i32; 3],
    bab:    i32,
    nat_armor:  i32,
    // feats:  Vec<Specials>,
}

impl Character {
    pub fn new() -> Character {
        Character {
            name:   "".to_string(),
            level:  vec![],
            size:   SizeClass::Medium,
            race:   Race::Human,
            abilities:  [10, 10, 10, 10, 10, 10],
            saves:  [0, 0, 0],
            bab:    0,
            nat_armor:  0,
        }
    }

    pub fn new_blank(char_name: &str, 
                     class_level: (Class, i32), 
                     char_race: Race) -> Character {
                     // alignment: Alignment) -> Character {
        Character {
            name:       char_name.to_string(),
            level:      vec![class_level],
            size:       SizeClass::Medium,
            race:       char_race,
            // align:      alignment,
            abilities:  [10, 10, 10, 10, 10, 10],
            saves:      [0, 0, 0],
            bab:        0,
            nat_armor:  0,
        }
    }

    pub fn apply_race(&mut self) {
        match self.race {
            Race::Dwarf    => { 
                self.size = SizeClass::Medium;
                self.abilities[2] += 2;
                self.abilities[5] -= 2;
            },
            Race::Elf      => {
                self.size = SizeClass::Medium;
                self.abilities[1] += 2;
                self.abilities[2] -= 2;
            },
            Race::Gnome    => {
                self.size = SizeClass::Small;
                self.abilities[2] += 2;
                self.abilities[0] -= 2;
            },
            Race::Goblin   => {
                self.size = SizeClass::Small;
                self.abilities[0] -= 2;
                self.abilities[1] += 2;
                self.abilities[5] -= 2;
            },
            Race::Gnoll   => {
                self.size = SizeClass::Medium;
                self.abilities[0] += 4;
                self.abilities[2] += 2;
                self.abilities[3] -= 2;
                self.abilities[5] -= 2;
                self.bab += 1;
                self.saves[0] += 3;
                self.nat_armor += 1;
            },
            Race::HalfElf => {
                self.size = SizeClass::Medium;
            },
            Race::Halfling => {
                self.size = SizeClass::Small;
                self.abilities[1] += 2;
                self.abilities[0] -= 2;
            },
            Race::HalfOrc => {
                self.size = SizeClass::Medium;
                self.abilities[0] += 2;
                self.abilities[3] -= 2;
                self.abilities[5] -= 2;
            },
            Race::Human    => {
                self.size = SizeClass::Medium;
            },
            Race::Kobold   => {
                self.size = SizeClass::Small;
                self.abilities[0] -= 4;
                self.abilities[1] += 2;
                self.abilities[2] -= 2;
                self.nat_armor += 1;
            },
            Race::LizardFolk => {
                self.size = SizeClass::Medium;
                self.abilities[0] += 2;
                self.abilities[2] += 2;
                self.abilities[3] -= 2;
                self.saves[1] += 3;
                self.nat_armor += 5;    // Damn!
            },
            Race::Minotaur => {
                self.size = SizeClass::Large;
                self.abilities[0] += 8;
                self.abilities[2] += 4;
                self.abilities[3] -= 4;
                if self.abilities[3] <= 3 { // Don't allow Int to go below 3
                    self.abilities[3] = 3;
                };
                self.abilities[5] -= 2;
                self.saves[0] += 2;
                self.saves[1] += 5;
                self.saves[2] += 5;
                self.nat_armor += 5;
            },
            Race::Ogre => {
                self.size = SizeClass::Large;
                self.abilities[0] += 10;
                self.abilities[1] -= 2;
                self.abilities[2] += 4;
                self.abilities[3] -= 4;
                self.abilities[5] -= 4;
                self.bab += 3;
                self.saves[0] += 4;
                self.saves[1] += 1;
                self.saves[2] += 1;
                self.nat_armor += 5;
            },
            Race::OgreMage => {
                self.size = SizeClass::Large;
                self.abilities[0] += 10;
                self.abilities[2] += 6;
                self.abilities[3] += 4;
                self.abilities[4] += 4;
                self.abilities[5] += 4;
                self.bab += 3;
                self.saves[0] += 4;
                self.saves[1] += 1;
                self.saves[2] += 1;
                self.nat_armor += 5;
            },
            Race::Orc => {
                self.size = SizeClass::Medium;
                self.abilities[0] += 4;
                self.abilities[3] -= 2;
                self.abilities[4] -= 2;
                self.abilities[5] -= 2;
            },
        }   // Close Match
    }   // Close fn apply_race

    pub fn apply_class(&mut self) {
        for n in self.level.iter() {
            let class_saves = get_base_saves(&n.0, n.1);
            let class_bab   = get_bab(&n.0, n.1);
            
            self.saves[0] += class_saves.0;
            self.saves[1] += class_saves.1;
            self.saves[2] += class_saves.2;

            self.bab += class_bab;
        }
    }

    pub fn add_class(&mut self, class: Class, lvl: i32) {
        // Check if class is present in the Vec<Class>:
        // If it is, then add the number
        // Else append the Class
        let mut x = false;
        for n in self.level.iter_mut() {
            // Ugly way to compare enums
            if mem::discriminant(&class) == mem::discriminant(&n.0) {
                x = true;
            }
            if x == true {
                // n is a &mut, so we want it to point
                // to the return value of add_level()
                n.1 += lvl;
            }
        }
        self.level.push( (class, lvl) );
    }

    /*
    // Get the ability mods of a character
    pub fn str_mod(&self) -> i32 { (self.abilities[0] - 10) / 2 }
    pub fn dex_mod(&self) -> i32 { (self.abilities[1] - 10) / 2 }
    pub fn con_mod(&self) -> i32 { (self.abilities[2] - 10) / 2 }
    pub fn int_mod(&self) -> i32 { (self.abilities[3] - 10) / 2 }
    pub fn wis_mod(&self) -> i32 { (self.abilities[4] - 10) / 2 }
    pub fn cha_mod(&self) -> i32 { (self.abilities[5] - 10) / 2 }
    */

    pub fn print(&self) {
        let mut s = "".to_string();
        for n in self.level.iter() {
            s = format!("{} | {}", s, class_to_string(&n.0, n.1) );
        }

        if self.name == "".to_string() {
            println!("{}{}", race_to_string(&(self.race)), s);
        } else {
            println!("{}, {}{}", self.name, race_to_string(&(self.race)), s);
        }
        println!("  Str: {} Dex: {} Con: {} Int: {} Wis: {} Cha: {}",
                 self.abilities[0], self.abilities[1], self.abilities[2],
                 self.abilities[3], self.abilities[4], self.abilities[5], );
        println!("  Fort: {}  Ref: {}  Will: {}",
                 self.saves[0], self.saves[1], self.saves[2] );
        println!("  Base Attack: {}", self.bab );
    }

}
