use std::env;
use std::process;

extern crate getopts;
use getopts::Options;

mod class;
use class::Class;
mod character;
use character::Character;
use character::Race;
use character::race_to_string;
use character::race_from_string;
// use character::Alignment;

// This program takes a series of arguments
// that represent the levels of dnd 3.5e classes
// that a player character or npc can take.
//
// Examples of well formed input are:
//
//   class wizard5
//
//   class fighter7
//
//   class bard6 rogue3
//
// Class numbers don't even need to be appended
// but do need to be preceeded by class
//
//   class barbarian 6  # This will work
//   class 6 barbarian` # This will not work
//
// So far only the base classes from the 3.5e
// player's handbook are supported

fn roll_4d6() -> (i32, i32, i32, i32, i32, i32) {
    let mut list = (0, 0, 0, 0, 0, 0);

    list
}

fn main() {
    let mut class_vec: Vec<Class> = Vec::new(); // Our goal is to build this
    let args: Vec<String> = env::args().collect();  // From this

    let mut level = 0;  // Reresents the total character level
    let mut skip = false;   // Used if class level is appended to 
                            // the actual class string

    // Start parsing stdin
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    // Declare option flags
    opts.optopt("n", "name",
                "Give the name of the character.",
                "NAME");
    opts.optopt("c", "class", "CLASS{, }LEVEL",
                "Name the class along how many levels. Only classes from the Player's Handbook 3.5e are supported. You many specify multiple classes, but each needs a specified amount of levels.");
    opts.optopt("r", "race", "RACE",
                "All of the races from the Player's Handbook 3.5e are supported, along with a few PC-able monsters from the Monster Manual I.");
    opts.optopt("a", "abilities", "'STR DEX CON INT WIS CHA'",
                "Input each of the six ability stats for a character. If not specified, then 4d6's are rolled and the highest three are picked for each ability score. If flag is omitted, then all 10's are assumed. Input May not be negative, or decimal.");
    opts.optflag("h", "help",
                 "Print helpful information, then exit.");

    // println!("{}", opts.usage("dnd-char") );
    println!("{}", args[1..].join(" "));
    match opts.parsing_style(getopts::ParsingStyle::StopAtFirstFree)
              .parse(&args[1..]) {
        Ok(m)  => {
            let mut pc = Character::new();
            let mut abilities = [0i32; 6];
            let mut race = Race::Human;
            // let mut name = "".to_string();
            // Check for name input
            let name = match m.opt_str("n") {
                Some(s) => s,
                None    => "".to_string(),
            };
            let mut class_vec: Vec<Class> = Vec::new();
            // Check for class input
            if m.opt_present("c") {
                let arg_class = m.opt_strs("c").join(" ");
                println!("{}", arg_class);
            }
            // Check for Race input
            if m.opt_present("r") {
               match  m.opt_str("r") {
                   Some(s) => match race_from_string(&s) {
                       Ok(r)  => { race = r; },
                       Err(s) => { println!("'{}' is not a supported race", s); },
                   }
                   None    => {},
               }

            }

            /*
            if m.opt_present("n") {

                match m.opt_str("n") {
                }
            }
            */
        },
        Err(err) => {},
    }

    /*
    let mut pc1 = Character::new_blank("Sulfjurn",
                                       Class::Bard(6),
                                       Race::Human, );
                                       // Alignment::ChaoticGood );
    pc1.add_class(Class::Rogue(3));
    pc1.add_class(Class::Bard(3));
    pc1.apply_class();
    pc1.apply_race();
    pc1.print();
    */

    /*
    for s in 1..args.len() {
        // If the previous term didn't have the class level appended 
        // to it, then we've already checked this &str, so skip it!
        if skip {
            skip = false;
            continue
        }

        // Find out where to split the given string in two
        // Based on where the first digit is located
        let mut index = 0i32;
        for n in args[s].chars() {
            if !n.is_numeric() { index += 1 }
        }

        // Split the string, if right is empty, we know we have
        // to check the next string for a class level
        let (left, right) = args[s].split_at(index as usize);

        // This stores the level of the current class, not the
        // total level of a character who multiclasses
        let lvl: i32;   

        if right.is_empty() {
            // Check if we were duped, and were given a class, but no level!
            if s + 1 == args.len() {
                eprintln!("Hey! {} wasn't specified a level!", left);
                process::exit(0x0100);
            }
            match args[s+1].parse::<i32>() {
            // If it can be parsed, then remeber to skip it to get to next class
                Ok(num) => { lvl = num; skip = true; },
                // crash politely if it cannot be parsed
                Err(_)  => { eprintln!("Sorry, but {} wasn't specified a level!", left);
                             process::exit(0x0100); },
            }
        } else {
            // If we don't have to worry about that mess, then just parse it!
            lvl = right.parse::<i32>().unwrap();
        }

        // Now we actually get to build the class_vec
        match left.to_lowercase().as_ref() {
            "barbarian" => { level += lvl; class_vec.push(Class::Barbarian(lvl)) },
            "bard"      => { level += lvl; class_vec.push(Class::Bard(lvl)) },
            "cleric"    => { level += lvl; class_vec.push(Class::Cleric(lvl)) },
            "druid"     => { level += lvl; class_vec.push(Class::Druid(lvl)) },
            "fighter"   => { level += lvl; class_vec.push(Class::Fighter(lvl)) },
            "monk"      => { level += lvl; class_vec.push(Class::Monk(lvl)) },
            "paladin"   => { level += lvl; class_vec.push(Class::Paladin(lvl)) },
            "ranger"    => { level += lvl; class_vec.push(Class::Ranger(lvl)) },
            "rogue"     => { level += lvl; class_vec.push(Class::Rogue(lvl)) },
            "sorcerer"  => { level += lvl; class_vec.push(Class::Sorcerer(lvl)) },
            "wizard"    => { level += lvl; class_vec.push(Class::Wizard(lvl)) },
            _ => { eprintln!("Sorry, but {} is not a supported class!", left);
                   process::exit(0x0100);
            },
        }

        if skip && s + 1 == args.len() {
            eprintln!("Hey! {} wasn't specified a level!", left);
            process::exit(0x0100);
        }
    }
    
    let mut base_attack = 0i32;
    let mut fort_save = 0i32;
    let mut ref_save  = 0i32;
    let mut will_save = 0i32;

    for v in class_vec {
        base_attack += class::get_bab(&v);
        let save_set = class::get_base_saves(&v);
        fort_save += save_set.0;
        ref_save  += save_set.1;
        will_save += save_set.2;
    }

    println!("Level {}\tBAB {}\tFort {}\tRef {}\tWill {}",
             level, base_attack, fort_save, ref_save, will_save);

    */

}
